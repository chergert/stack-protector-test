#!/bin/sh

make

echo "==========================="

echo "Testing with stack protector"
for i in `seq 0 5`; do
	time ./test
done

echo ""
echo "Testing without stack protector"
for i in `seq 0 5`; do
	time ./test-no-stack-protector
done


echo "==========================="
