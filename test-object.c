#include <gio/gio.h>

G_BEGIN_DECLS

#define TEST_TYPE_OBJECT (test_object_get_type())

#ifdef _NO_STACK_PROTECTOR
__attribute__((optimize("no-stack-protector")))
#endif
G_DECLARE_FINAL_TYPE (TestObject, test_object, TEST, OBJECT, GObject)

G_END_DECLS

struct _TestObject
{
  GObject parent;
};

G_DEFINE_TYPE_EXTENDED (TestObject, test_object, G_TYPE_OBJECT, 0,
                        G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, NULL))

static void
test_object_class_init (TestObjectClass *klass)
{
}

static void
test_object_init (TestObject *self)
{
}

int
main (int argc,
      char *argv[])
{
  GType type_id = test_object_get_type();

  for (guint i = 0; i < 1000000000; i++)
    g_assert (type_id == test_object_get_type());

  return 0;
}
