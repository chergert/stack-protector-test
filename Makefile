all: test test-no-stack-protector

# export-dynamic just to make sure we get a symbol to objdump and verify
CFLAGS = -export-dynamic -O2 -fomit-frame-pointer -fstack-protector-strong

test: test-object.c Makefile
	$(CC) $(CFLAGS) test-object.c -o $@ $(shell pkg-config --cflags --libs gio-2.0)

test-no-stack-protector: test-object.c Makefile
	$(CC) $(CFLAGS) -D_NO_STACK_PROTECTOR test-object.c -o $@ $(shell pkg-config --cflags --libs gio-2.0)

clean:
	rm -f test test-no-stack-protector
